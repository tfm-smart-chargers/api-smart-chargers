'use strict'

require('dotenv').config()
try {
    const app = require('./app')
    const config = require('./config')

    app.listen(config.port, () => {
      console.log(`API corriendo en http://localhost:${config.port}`)
    })
} catch(err) {
    console.log(`Error while initiating app: ${err}`)
    console.log(`Stack trace: ${err.stack}`)
}

