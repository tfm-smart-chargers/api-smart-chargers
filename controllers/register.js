'use strict'

const con = require('../db')

function getRegisters (req, res) {
    console.log(`CarPlate ${req.params.carPlate} has requested registers`);
    let carPlate = req.params.carPlate

    let subscriberIds = [];

    con.query("SELECT * FROM subscriber WHERE car_plate = ?",[carPlate], (err, subscriber) => {
        if (err) {
            console.log(`Error retrieving subscriber: ${err}`);
            return res.status(500);
        }
        if(!subscriber || subscriber.length === 0) {
            console.log(`Subscriber was not found`)
            return res.status(404).send({ message: 'The subscriber does not exist' })
        }
        console.log("Subscriber found");
        subscriber.forEach(el => {
            subscriberIds.push(el.id);
        });
        
        var result = []; 
        // Query returns only the newest record
        con.query("SELECT * FROM register WHERE ((" + getSubscriberQuery(subscriberIds) + ") AND flag = 2) ORDER BY id DESC LIMIT 1", (err, register) => {
            if (err) {
                console.log(`Error retrieving registers: ${err}`);
                return res.status(500);
            }
            if(!register || register.length === 0) {
                console.log(`Registers were not found`);
            } else {
                console.log("Registers found");
                result.push(register[0]);
            }
        })

        con.query("SELECT * FROM register WHERE ((" + getSubscriberQuery(subscriberIds) + ") AND (flag = 0 OR flag = 1)) ORDER BY id DESC LIMIT 1", (err, register) => {
            if (err) {
                console.log(`Error retrieving registers: ${err}`);
                return res.status(500).send({ message: 'Server internal error' });
            }
            if(!register || register.length === 0) {
                console.log(`Registers were not found`);
                return res.status(404).send({ message: 'There are not registers' });
            }
            console.log("Registers found");
            result.push(register[0]);
            res.status(200).send(result);
        })
    })
}

function getSubscriberQuery(subscriberIds) {
    var query = "";
    var c = 0;
    while(c < subscriberIds.length) {
        if(c === 0) {
            query += `subscriber_id = ${subscriberIds[c]} `;
        } else {
            query += `OR subscriber_id = ${subscriberIds[c]}`
        }
        c += 1;
    }
    console.log(query);
    return query;
}

module.exports = {
    getRegisters
}
