const mysql = require('mysql')
const config = require('./config')

let connection = mysql.createConnection({
    connectionLimit: 1,
    host: config.sql.host,
    port: config.sql.port,
    user: config.sql.username,
    password: config.sql.password,
    database: config.sql.name,
    multipleStatements: true
});
  
connection.connect(function(err) {
    if (err) {
    console.error('Error connecting: ' + err.stack);
    return;
    }
    console.log('Connected as thread id: ' + connection.threadId);
});

module.exports = connection