'use strict'

const express = require('express')

const subscriberCtrl = require('../controllers/subscriber')
const registerCtrl = require('../controllers/register')

const api = express.Router()

api.get('/subscriber/:carPlate', subscriberCtrl.getSubscriber)

api.get('/registers/:carPlate', registerCtrl.getRegisters)

module.exports = api
