const convict = require('convict')

const config = convict({
  env: {
    format: ['prod', 'dev', 'test'],
    default: 'dev',
    arg: 'nodeEnv',
    env: 'NODE_ENV'
  },
  port: {
    format: 'port',
    default: 8080,
    arg: 'port',
    env: 'PORT'
  },
  sql: {
    host: {
      format: '*',
      default: 'localhost',
      arg: 'db_host',
      env: 'DB_HOST'
    },
    port: {
      format: 'port',
      default: 3307,
      arg: 'db_port',
      env: 'DB_PORT'
    },
    name: {
      format: String,
      default: 'smart_chargers',
      arg: 'db_name',
      env: 'DB_NAME'
    },
    username: {
      format: String,
      default: 'username',
      arg: 'db_username',
      env: 'DB_USERNAME'
    },
    password: {
      format: String,
      default: 'password',
      arg: 'db_password',
      env: 'DB_PASSWORD'
    }
  }
})

const env = config.get('env')
config.loadFile(`./config/${env}.json`)

config.validate({ allowed: 'strict' })

module.exports = config.getProperties()
