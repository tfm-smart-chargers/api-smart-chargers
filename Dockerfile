FROM node:9
ARG NODE_ENV=prod
ENV NODE_ENV=${NODE_ENV}
WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN npm install
EXPOSE 8080
CMD [ "npm", "start" ]