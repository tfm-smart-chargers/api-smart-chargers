'use strict'

const con = require('../db')

function getSubscriber (req, res) {
    console.log(`Subscriber requested with car plate: ${req.params.carPlate}`);
    let carPlate = req.params.carPlate

    con.query("SELECT * FROM subscriber WHERE car_plate = ?",[carPlate], (err, subscriber) => {
        if (err) {
        console.log(`Error retrieving subscriber: ${err}`);
        }
        if(!subscriber || subscriber.length === 0) {
            console.log(`Subscriber was not found`)
            return res.status(404).send({ message: 'The subscriber does not exist' })
        }
        console.log("Subscriber found");
        res.status(200).send(subscriber)
    })
}

module.exports = {
    getSubscriber
}
